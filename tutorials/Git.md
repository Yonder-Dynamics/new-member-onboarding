# Git

Git is an example of a distributed version control system (DVCS) commonly used for open source and commercial software development. DVCSs allow full access to every file, branch, and iteration of a project, and allows every user access to a full and self-contained history of all changes. Unlike once popular centralized version control systems, DVCSs like Git don’t need a constant connection to a central repository. Developers can work anywhere and collaborate asynchronously from any time zone.

Without version control, team members are subject to redundant tasks, slower timelines, and multiple copies of a single project. To eliminate unnecessary work, Git and other VCSs give each contributor a unified and consistent view of a project, surfacing work that’s already in progress. Seeing a transparent history of changes, who made them, and how they contribute to the development of a project helps team members stay aligned while working independently.

New Members should be familiar with using `git` and look up [documentation](https://services.github.com/on-demand/downloads/github-git-cheat-sheet/) for help.

`.gitignore` files allow users to not push code to the repository. This is especially important for statically typed languages as compiling them creates files that aren't necessary to upload (Ex: .pyc .class .o ).