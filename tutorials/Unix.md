# Unix

The console is a tool that easily interfaces with the kernel. You can perform various commands such as making a file or folder. Here is a [link](https://files.fosswire.com/2007/08/fwunixref.pdf) to a list of some useful commands.